<span style="width:753px !important;" >{{ErrorMessage}} {{SuccessMessage}}</span ><br >
<div id="column1" class="column" >
	<div class="habblet-container " >
		<div class="cbb clearfix orange " >
			<h2 class="title" >get {{hotel_name}} Credits</h2 >

			<div class="method-group online clearfix" >
				<div class="method idx0" >
					<div class="method-content" >
						<h2 >Free credits!</h2 >

						<div class="summary clearfix" >
							{{hotel_name}} Credits are completely FREE, and always will be. On registration, you get
							15,000 for free, awesome bro!
						</div >
					</div >
					<div class="smallprint" >
						<style
							type="text/css" >.method-group.online .method.idx0 h2 { font-size: 26px !important; }</style >
					</div >
				</div >
				<div class="method idx1 nosmallprint" >
					<div class="method-content" >
						<h2 >How do I gain more credits?</h2 >

						<div class="summary clearfix" >
							<div id="credits-countdown" >
								You can gain more {{hotel_name}} credits by either buying VIP or using your current
								amount and playing the economy in the hotel.
							</div >
						</div >
					</div >
				</div >
			</div >
			<script type="text/javascript" >
				document.observe ("dom:loaded", function () { new CreditsList (); });
			</script >
			<div class="disclaimer" style="border: 0 !important;" >
				<h3 ><span >Do you have any questions?</span ></h3 >
				Are you out of credits? That's weird! We have events in the hotel constantly being run by our
				staff<br />
				where you can win rare furniture and coins. You can also go to one of the hotel casinos and try your
				luck there.<br />
				It's really quite fun to play your luck at a casino, so give it a try!<br /><br />
				<strong >If you have any more questions, feel free to contact a member of our staff.</strong >
			</div >
		</div >
	</div >
	<script
		type="text/javascript" >if (! $ (document.body).hasClassName ('process-template')) { Rounder.init (); }</script >
</div >
</div>
<script type="text/javascript" >
	HabboView.run ();
</script >
</div>
<?= Page::Footer (); ?>
</div>
</div>
</div>