<script src="//cdn.ckeditor.com/4.4.6/standard/ckeditor.js" ></script >
<div id="change-password-form" class="overlay-dialog" style="left: 40%; top: 80px;" >
	<div id="change-password-form-container" class="clearfix form-container" >
		<h2 id="change-password-form-title" class="bottom-border" >Create a New Article</h2 >

		<div id="change-password-form-content" style="" >
			<style >.email-address { font-size: 18px; font-weight: bold; height: 30px; width: 330px; text-align: center; color: black; border: 1px solid #633; }</style >
			<div id="change-password-form-content" >
				<span >{{SuccessMessage}} {{ErrorMessage}}<br ></span >
				<?php
				if (!isset($_GET['v'])):
				?>
				<form action="{{hotel_url}}/createnews/postarticle/?v=true" method="post" id="forgotten-pw-form" >
					<span >Type the Title</span >

					<div id="email" class="center bottom-border" >
						<input tabindex="1" type="text" name="title" id="change-password-email-address"
						       class="email-address" style="width:320px;" />
					</div >
					<span >Type the Image Url</span >

					<div id="email" class="center bottom-border" >
						<input tabindex="1" type="text" name="image" id="change-password-email-address"
						       class="email-address" style="width:320px;" />
					</div >
					<span >Type The Text</span >

					<div id="email" class="center bottom-border" >
						<textarea name="text" id="change-password-email-address" class="email-address"
						          style="width:320px;height:100px;" ></textarea >
						<script >
							CKEDITOR.replace (
								'text', {
									language: 'en',
									width: 320,
									height: 145
								}
							);</script >
					</div >
			</div >
			<p >&nbsp;</p >

			<div class="change-password-buttons" >
				<a href="{{hotel_url}}/me/" id="change-password-cancel-link" >Cancel</a >
				<a href="javascript:;" onclick="document.getElementById('forgotten-pw-form').submit();"
				   id="change-password-submit-button" class="new-button" ><b >Ok</b ><i ></i ></a >
				</form>
				<?php
				else:
					?>
					<a href="{{hotel_url}}/me/" id="change-password-cancel-link" >Fechar</a >
				<?php
				endif;
				?>
			</div >
		</div >
	</div >
	<div id="change-password-form-container-bottom" class="form-container-bottom" ></div >
</div >
<script type="text/javascript" >
	function initChangePasswordForm () { ChangePassword.init (); }
	if (window.HabboView) { HabboView.add (initChangePasswordForm); }
	else if (window.habboPageInitQueue) { habboPageInitQueue.push (initChangePasswordForm); }
</script >