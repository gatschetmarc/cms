<span style="width:753px !important;" >{{ErrorMessage}} {{SuccessMessage}}</span ><br >
<div id="column1" class="column" >
	<div class="habblet-container " >
		<div id="habbo-way-content" >
			<table >
				<tbody >
				<tr >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_0.png" alt="" > <br >
					</td >
					<td >
						<h4 >Protect Your Personal Info</h4 >
						You never know who you're really speaking to online, so never give out your real name, address,
						phone numbers, photos and school. Giving away your personal info could lead to you being
						scammed, bullied or put in danger.
					</td >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_1.png" alt="" > <br >
					</td >
					<td >
						<h4 >Protect Your Privacy</h4 >
						Keep your Skype, MSN, Facebook details private. You never know where it might lead you.
					</td >
				</tr >
				<tr >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_2.png" alt="" > <br >
					</td >
					<td >
						<h4 >Don't Give In To Peer Pressure</h4 >
						Just because everyone else seems to be doing it, if you're not comfortable with it, don't do it!
					</td >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_3.png" alt="" > <br >
					</td >
					<td >
						<h4 >Keep Your Pals In Pixels</h4 >
						Never meet up with people you only know from the internet, people aren't always who they claim
						to be. If someone asks you to meet with them in real life say "No thanks!" and tell a moderator,
						your parents or another trusted adult.
					</td >
				</tr >
				<tr >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_4.png" alt="" > <br >
					</td >
					<td >
						<h4 >Don't Be Scared To Speak Up</h4 >
						If someone is making you feel uncomfortable or scaring you with threats in Habbo, report them
						immediately to a moderator using the Panic Button.
					</td >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_5.png" alt="" > <br >
					</td >
					<td >
						<h4 >Ban The Cam</h4 >
						You have no control over your photos and webcam images once you share them over the internet and
						you can't get them back. They can be shared with anyone, anywhere and be used to bully or
						blackmail or threaten you. Before you post a pic, ask yourself, are you comfortable with people
						you don't know viewing it?
					</td >
				</tr >
				<tr >
					<td >
						<img src="//habboo-a.akamaihd.net/c_images/safetyquiz/page_6.png" alt="" > <br >
					</td >
					<td >
						<h4 >Be A Smart Surfer</h4 >
						Websites that offer you free Credits, Furni, or pretend to be new Habbo Hotel or Staff homepages
						are all scams designed to steal your password. Don't give them your details and never download
						files from them; they could be keyloggers or viruses!
					</td >
				</tr >
				</tbody >
			</table >
		</div >
	</div >
	<script
		type="text/javascript" >if (! $ (document.body).hasClassName ('process-template')) { Rounder.init (); }</script >
</div >
<script type="text/javascript" >
	HabboView.run ();
</script >
</div>
<?= Page::Footer (); ?>
</div>
</div>