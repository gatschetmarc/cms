<span style="width:753px !important;" >{{ErrorMessage}} {{SuccessMessage}}</span ><br >
<div id="column1" class="column" >
	<div class="habblet-container " >
		<div class="cbb clearfix blue " >
			<h2 class="title" >Donation Information</h2 >

			<div id="habboclub-info" class="box-content" >
				<img src="{{gallery_url}}/v2/images/geschenke.gif" align="right" vspace="5" hspace="5" >

				<p >

				<h3 ><font color='darkred' >Why donate?</h3 ><font color='black' >
					Fame Hotel runs on donations from users like you! Without users constantly donating and supporting
					us, we would not be able to stay online. In addition to knowing that Fame Hotel will be able to live
					on even longer, you can also get some pretty cool rewards for donating! Look at the list to the
					right to see those rewards.
					<h3 ><font color='darkred' >How do I donate?</h3 ><font color='black' >
						Donating is similar to purchasing VIP. The only payment method that is available for donating is
						Paypal. Simply scroll to the bottom of the page, select the donation amount, enter in your Fame
						Hotel username, and click pay now! Paypal is the leading and most widely-used payment system on
						the internet. After donating through Paypal, you can expect to have your credits, pixels, badge,
						and rares delivered to you instantly, or at most a few minutes. The rares will be sent to you in
						Red Presents. You can simply place these presents down and open them to receive your rares.
						<h3 ><font color='darkred' >Can I donate more than once?</h3 ><font color='black' >
							You can donate as many times as you like! Donate whenever you can -- we can always use more
							funds to help pay for our multiple servers, domains, and various other services which we
							provide free of charge.
							<h3 ><font color='darkred' >Refunds?</h3 ><font color='black' >
								Refunds are not allowed. <b >All payments are final.</b > By donating to Fame Hotel, you
								agree that you will not attempt to refund your donation at any time. This causes a huge
								hassle for us, and we <b >will</b > ban your account if you attempt to get your money
								back.<p >

								<h3 ><font color='darkred' >What color rares are available?</h3 ><font color='black' >
									<b >Ice Cream Machines</b > - <i >Aqua, Blue, Choco, Fucsia, Gold, Ochre, Pink,
									                                  Purple, Red, Shamrock</i ><br >
									<b >Slurpee Machines</b > - <i >Aqua, Blueberry, Cherry, Grape, Lime, Orange,
									                                Pink</i ><br >
									<b >Arcade Machines</b > - <i >Red, Yellow</i ><br ><br >
									                            All rare colors will be randomly selected. You have an
									                            equal chance to receive all colors of rares!
									<br ><br >

									<p >So why wait?! Donate now, and get some really cool rewards!</p ><font
										color='black' >
			</div >
		</div >
	</div >
	<script
		type="text/javascript" >if (! $ (document.body).hasClassName ('process-template')) { Rounder.init (); }</script >
	<div class="habblet-container " >
		<div class="cbb clearfix white " style="padding: 15px;" >
			<center ><a
					onclick="window.open('/buy_donate.php','popup','width=900,height=800,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"
					href="{{paypalurl}}" target="_blank" class="new-button habboid-submit" style="float: left;" ><b >Pay
			                                                                                                         By
			                                                                                                         Paypal</b ><i ></i ></a >
			</center >
			</p>
		</div >
	</div >
	<script
		type="text/javascript" >if (! $ (document.body).hasClassName ('process-template')) { Rounder.init (); }</script >
</div >
<div id="column2" class="column" >
	<div class="habblet-container " >
		<div class="cbb clearfix settings " >
			<h2 class="title" >Donation Benefits</h2 >

			<div id="habboclub-info" class="box-content" >
				<p ><strong ><img align="middle" src="{{gallery_url}}/v2/images/PX00.gif" alt="" width="40"
				                  height="40" />Level 1 (<em >�1.00) </em ></strong ><br >Donator's Badge (Level 1),
				                                                                          10,000 Credits & 5000 pixels
				</p >

				<p ><strong ><span style="font-weight: normal;" ><strong ><img align="middle"
				                                                               src="{{gallery_url}}/v2/images/PX01.gif"
				                                                               alt="" width="40" height="40" />Level 2 (<em >�3.00)
				                                                                                                             &nbsp;</em ></strong ><br >Donator's Badge (Level 2), , 10000 credits, 3000 pixels</span ></strong >
				</p >

				<p ><strong ><span style="font-weight: normal;" ><strong ><img align="middle"
				                                                               src="{{gallery_url}}/v2/images/PX02.gif"
				                                                               alt="" width="40" height="40" />Level 3 (<em >�5.00)
				                                                                                                             &nbsp;</em ></strong ><br >Donator's Badge (Level 3), , 20000 credits, 6000 pixels, 1x any color ice-cream machine</span ></strong >
				</p >

				<p ><strong ><span style="font-weight: normal;" ><strong ><img align="middle"
				                                                               src="{{gallery_url}}/v2/images/PX03.gif"
				                                                               alt="" width="40" height="40" />Level 4 (<em >�7.00)
				                                                                                                             &nbsp;</em ></strong ><br >Donator's Badge (Level 4), , 40000 credits, 13000 pixels, 2x any color ice-cream machine, 1x throne</span ></strong >
				</p >

				<p ><img align="middle" src="{{gallery_url}}/v2/images/PX04.gif" alt="" width="40"
				         height="40" /><strong ><span style="font-weight: normal;" ><strong >Level 5 (<em >�9.00)
				                                                                                           &nbsp;</em ></strong ><br >Donator's Badge (Level 5), , 60000 credits, 20000 pixels, 3x any color ice-cream machine, 2x throne, 1x lollipop stand</span ></strong >
				</p >

				<p ><strong ><span style="font-weight: normal;" ><strong ><img align="middle"
				                                                               src="{{gallery_url}}/v2/images/PX05.gif"
				                                                               alt="" width="40"
				                                                               height="40" /></strong ></span ></strong ><strong ><span
							style="font-weight: normal;" ><strong >Level 6 (<em > �15.00) &nbsp;</em ></strong ><br >Donator's Badge (Level 6), , 80000 credits, 26000 pixels, 4x any color ice-cream machine, 3x throne, 1x lollipop stand, 1x any color slurpee machine</span ></strong >
				</p >

				<p ><strong ><span style="font-weight: normal;" ><strong ><img align="middle"
				                                                               src="{{gallery_url}}/v2/images/PX06.gif"
				                                                               alt="" width="40"
				                                                               height="40" /></strong ></span ></strong ><strong ><span
							style="font-weight: normal;" ><strong >Level 7 (<em >�18.00) &nbsp;</em ></strong ><br >Donator's Badge (Level 7), , 100000 credits, 33000 pixels, 5x any color ice-cream machine, 4x throne, 1x lollipop stand, 1x any color slurpee machine, 1x arcade machine</span ></strong >
				</p >
			</div >
		</div >
	</div >
	<script type="text/javascript" >
		Pngfix.doPngImageFix ();
	</script >
	<div id="footer" >
		<div style="float: none;" >
		</div >
		<div style="float: right;" >
		</div >
		<div style="clear: both;" ></div >
	</div >
</div >
</div></div>
<script type="text/javascript" >
	HabboView.run ();
</script >
</div>
<?= Page::Footer (); ?>
</div>
</div>
</div>