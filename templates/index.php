<div class="cbb process-template-box clearfix"
     style="border-radius: 6px;border-bottom: 3px solid rgb(150, 179, 190);width: 820px;height: 600px;background: url('http://www.ihabbo.nu/img/bg.png') no-repeat right bottom white;" >
	<div id="content" >
		<div id="header" class="clearfix" >
			<h1 ><a href="{{hotel_url}}"
			        style="color: #008D49;font-family: 'open Sans', sans-serif;font-weight: lighter;font-size: 35px;text-decoration: none;background-image: none;text-indent: 0;" >{{hotel_name}}</a >
			</h1 >
			<ul class="stats" >
				<li class="stats-online" ><span class="stats-fig" >{{online_users}}</span > Users Online</li >
				<li class="stats-visited" ><span class="stats-fig" >{{registered_users}}</span > Registered Users</li >
			</ul >
		</div >
		<div id="process-content" >
			<div id="column2" class="column" >
				<div class="habblet-container " id="create-habbo" >
					<div id="create-habbo-flash" >
						<div id="create-habbo-nonflash" >
							<div id="landing-register-text" ><a
									href="javascript:window.location.href='{{hotel_url}}' + 'p?=register'" ><span >Join now, it's free</span ></a >
							</div >
							<div id="landing-promotional-text" ><span >Habbo is a virtual world where you can meet and make friends.</span >
							</div >
						</div >
						<div class="cbb clearfix green" id="habbo-intro-nonflash" >
							<h2 class="title" >To get most out of Habbo, do this:</h2 >

							<div class="box-content" >
								<ul >
									<li id="habbo-intro-install" style="display:none" ><a
											href="http://www.adobe.com/go/getflashplayer" >Install Flash Player 8 or
									                                                       higher</a ></li >
									<noscript >
										<li >Enable JavaScript</li >
									</noscript >
								</ul >
							</div >
						</div >
					</div >
					<script type="text/javascript" >
						var swfobj = new SWFObject ("{{gallery_url}}flash/intro/habbos.swf", "ch", "396", "378", "8");
						swfobj.addParam ("AllowScriptAccess", "always");
						swfobj.addParam ("wmode", "transparent");
						swfobj.addVariable ("base_url", "{{gallery_url}}flash/intro/");
						swfobj.addVariable ("habbos_url", "{{hotel_url}}/action/promo/");
						swfobj.addVariable ("create_button_text", "Register Today!");
						swfobj.addVariable ("in_hotel_text", "Online now!");
						swfobj.addVariable ("slogan", "Habbo");
						swfobj.addVariable ("video_start", "PLAY VIDEO");
						swfobj.addVariable ("video_stop", "STOP VIDEO");
						swfobj.addVariable ("button_link", "{{hotel_url}}/register/");
						swfobj.addVariable ("localization_url", "{{gallery_url}}flash/intro/landing_intro.xml");
						swfobj.addVariable ("video_link", "{{gallery_url}}flash/intro/Habbo_intro.swf");
						swfobj.write ("create-habbo-flash");
						HabboView.add (
							function () {
								if (deconcept.SWFObjectUtil.getPlayerVersion ()["major"] >= 8) {
									try { $ ("habbo-intro-nonflash").hide (); } catch (e) {}
								}
								else {
									$ ("habbo-intro-install").show ();
								}
							}
						);
						var PromoHabbos = {track: function (n) { if (! ! n && window.pageTracker) { pageTracker._trackPageview ("/landingpromo/" + n); } }}
					</script >
				</div >
				<script type="text/javascript" >if (! $ (document.body).hasClassName (\
					'process-template\')) { Rounder.init(); }</script >
			</div >
			<div id="column3" class="column" style="width: 158px;margin-top: -8px;" >
				<div class="cbb loginbox clearfix"
				     style="border-radius: 3px;border: 1px solid rgb(204, 204, 204);border-bottom: 3px solid rgb(204, 204, 204);" >
					<h2 class="title" style="margin: 4px;border-radius: 3px;" >Login</h2 >

					<div class="box-content clearfix" id="login-habblet" >
						<form action="{{hotel_url}}/action/login/" method="post" class="login-habblet" >
							<span >{{ErrorMessage}}</span >
							<ul >
								<li >
									<label for="login-username" class="login-text" >Username</label >
									<input tabindex="1" type="text" class="login-field" name="username"
									       id="login-username" />
								</li >
								<li >
									<label for="login-password" class="login-text" >Password</label >
									<input tabindex="2" type="password" class="login-field" name="password"
									       id="login-password" />
									<input type="submit" style="display:none;" value="Sign in" class="submit"
									       id="login-submit-button" />
								</li >
								<li class="no-label" style="margin-left: 22px;padding:0px;margin-top: 45px;" >
									<a href="javascript:window.location.href='{{hotel_url}}/register/'"
									   class="login-register-link" ><span >Register</span ></a >
								</li >
							</ul >
						</form >
					</div >
				</div >
			</div >
			<div id="remember-me-notification" class="bottom-bubble" style="display:none;" >
				<div class="bottom-bubble-t" >
					<div ></div >
				</div >
				<div class="bottom-bubble-c" >
					By selecting remember me you will stay signed in on this computer until you click Sign Out. If this
					is a public computer please do not use this feature.
				</div >
				<div class="bottom-bubble-b" >
					<div ></div >
				</div >
			</div >
			<script type="text/javascript" >
				HabboView.add (LoginFormUI.init);
				HabboView.add (RememberMeUI.init);
			</script >
		</div >
	</div >
</div >

<script type="text/javascript" >
	HabboView.run ();
</script >