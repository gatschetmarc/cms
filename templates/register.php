<div id="change-password-form" class="overlay-dialog" style="left: 40%; top: 80px;" >
	<div id="change-password-form-container" class="clearfix form-container" >
		<h2 id="change-password-form-title" class="bottom-border" >Register on {{hotel_name}}</h2 >

		<div id="change-password-form-content" style="" >
			<style >.email-address { font-size: 18px; font-weight: bold; height: 30px; width: 330px; text-align: center; color: black; border: 1px solid #633; }</style >
			<form action="{{hotel_url}}/register/register/" method="post" id="forgotten-pw-form" >
				<div id="change-password-form-content" >
					<span >{{ErrorMessage}}</span ><br >
					<span >Type your Username</span >

					<div id="email" class="center bottom-border" >
						<input tabindex="1" type="text" name="username" id="change-password-email-address"
						       class="email-address" maxlength="48" style="width:200px;" />
					</div >
					<span >Type your Mail</span >

					<div id="email" class="center bottom-border" >
						<input tabindex="1" type="text" name="mail" id="change-password-email-address"
						       class="email-address" maxlength="48" style="width:200px;" />
					</div >
					<span >Type your Password</span >

					<div id="email" class="center bottom-border" >
						<input tabindex="2" type="password" name="password" id="change-password-email-address"
						       class="email-address" maxlength="48" style="width:200px;" />
					</div >
				</div >
				<p ></p >

				<div class="change-password-buttons" >
					<a href="{{hotel_url}}" id="change-password-cancel-link" >Cancel</a >
					<a href="javascript:;" onclick="document.getElementById('forgotten-pw-form').submit();"
					   id="change-password-submit-button" class="new-button" ><b >Ok</b ><i ></i ></a >
			</form >
		</div >
	</div >
</div >
<div id="change-password-form-container-bottom" class="form-container-bottom" ></div >
</div>
<script type="text/javascript" >
	function initChangePasswordForm () { ChangePassword.init (); }
	if (window.HabboView) { HabboView.add (initChangePasswordForm); }
	else if (window.habboPageInitQueue) { habboPageInitQueue.push (initChangePasswordForm); }
</script >