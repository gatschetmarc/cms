<script type="text/javascript" >
	document.habboLoggedIn = true;
	var habboName = "{{user_name}}
	var habboId = {
	{user_id}
	}
	;
	var facebookUser = true;
	var habboReqPath = "
	var habboStaticFilePath = "{{gallery_url}}
	var habboImagerUrl = "{{hotel_url}}habbo-imaging/
	var habboPartner = "
	var habboDefaultClientPopupUrl = "{{hotel_url}}{{client_name}}
	window.name = "
	if (typeof HabboClient != "undefined") {
		HabboClient.windowName = "
		HabboClient.maximizeWindow = true;
	}
</script >

<link rel="stylesheet" href="{{gallery_url}}static/styles/habboflashclient.css" type="text/css" />
<script src="{{gallery_url}}static/js/habboflashclient.js" type="text/javascript" ></script >
<script src="{{gallery_url}}static/js/identity.js" type="text/javascript" ></script >
<script type="text/javascript" >
	FlashExternalInterface.loginLogEnabled = true;
	FlashExternalInterface.logLoginStep ("web.view.start");
	if (top == self) { FlashHabboClient.cacheCheck (); }
	var flashvars = {
		"client.allow.cross.domain": "0",
		"client.notify.cross.domain": "1",
		"connection.info.host": "{{emu_ip}}",
		"connection.info.port": "{{emu_port}}",
		"site.url": "{{hotel_url}}",
		"url.prefix": "{{swf_url}}",
		"client.reload.url": "{{hotel_url}}{{client_name}}",
		"client.fatal.error.url": "{{hotel_url}}/fproblem/",
		"client.connection.failed.url": "{{hotel_url}}/fproblem/",
		"logout.url": "{{hotel_url}}{{client_name}}",
		"logout.disconnect.url": "{{hotel_url}}{{client_name}}",
		"external.variables.txt": "{{swf_url}}gamedata/external_variables/{{comb_name}}",
		"external.texts.txt": "{{swf_url}}gamedata/external_flash_texts/{{comb_name}}",
		"external.figurepartlist.txt": "{{swf_url}}gamedata/figuredata/{{comb_name}}",
		"external.override.texts.txt": "{{swf_url}}gamedata/override/external_flash_override_texts/{{comb_name}}",
		"external.override.variables.txt": "{{swf_url}}gamedata/override/external_override_variables/{{comb_name}}",
		"productdata.load.url": "{{swf_url}}gamedata/productdata/{{comb_name}}",
		"furnidata.load.url": "{{swf_url}}gamedata/furnidata_xml/{{comb_name}}",
		"sso.ticket": "{{client_ticket}}",
		"processlog.enabled": "1",
		"account_id": "{{user_id}}",
		"client.starting": "Please wait! {{hotel_name}} is starting up.",
		"flash.client.url": "{{swf_url}}gordon/{{gordon_path}}",
		"user.hash": "{{user_hash}}",
		"facebook.user": "0",
		"has.identity": "1",
		"flash.client.origin": "popup",
		"nux.lobbies.enabled": "true",
	};
	var params = {
		"base": "{{swf_url}}gordon/{{gordon_path}}",
		"allowScriptAccess": "always",
		"menu": "false"
	};
	if (! (HabbletLoader.needsFlashKbWorkaround ())) {
		params["wmode"] = "opaque }
		FlashExternalInterface.signoutUrl = "{{hotel_url}}/logout/
		var clientUrl = "{{swf_url}}gordon/{{gordon_path}}{{swf_name}}
		swfobject.embedSWF (clientUrl, "flash-container", "100%", "100%", "11.1.0", "{{gallery_url}}flash/expressInstall.swf", flashvars, params, null, FlashExternalInterface.embedSwfCallback);
		window.onbeforeunload = unloading;
		function unloading () {
			var clientObject;
			if (navigator.appName.indexOf ("Microsoft") != - 1)
				clientObject = window["flash-container"];
			else
				clientObject = document["flash-container"];
			clientObject.unloading ();
		}

		window.onresize = function () { HabboClient.storeWindowSize (); }.debounce (0.5);
</script >
<meta name="build" content="63-BUILD-FOR-PATCH-2898b - 18.11.2014 16:50 - com" />
</head>
<body id="client" class="flashclient" >
<div id="overlay" ></div >
<img src="{{gallery_url}}v2/images/page_loader.gif" style="position:absolute; margin: -1500px;" />

<div id="overlay" ></div >
<div id="client-ui" >
	<div id="flash-wrapper" >
		<div id="flash-container" >
			<div id="content" style="width: 400px; margin: 20px auto 0 auto; display: none" >
				<div class="cbb clearfix" >
					<h2 class="title" >Please update your Flash Player to the latest version.</h2 >

					<div class="box-content" >
						<p >You can install and download Adobe Flash Player here: <a
								href="http://get.adobe.com/flashplayer/" >Install flash player</a >. More instructions
						    for installation can be found here: <a
								href="http://www.adobe.com/products/flashplayer/productinfo/instructions/" >More
						                                                                                    information</a >
						</p >

						<p ><a href="http://www.adobe.com/go/getflashplayer" ><img
									src="{{gallery_url}}v2/images/client/get_flash_player.gif"
									alt="get Adobe Flash player" /></a ></p >
					</div >
				</div >
			</div >
			<script type="text/javascript" >
				$ ('content').show ();
			</script >
			<noscript >
				<div style="width: 400px; margin: 20px auto 0 auto; text-align: center" >
					<p >If you are not automatically redirected, please <a href="/client/nojs" >click here</a ></p >
				</div >
			</noscript >
		</div >
	</div >
	<div id="content" class="client-content" ></div >
	<iframe id="game-content" class="hidden" allowtransparency="true" frameBorder="0" src="about:blank" ></iframe >
	<iframe id="page-content" class="hidden" allowtransparency="true" frameBorder="0" src="about:blank" ></iframe >
</div >
<script type="text/javascript" >
	RightClick.init ("flash-wrapper", "flash-container");
	if (window.opener && window.opener != window && window.opener.location.href == "/") {
		window.opener.location.replace ("/me");
	}
	$ (document.body).addClassName ("js");
	HabboClient.startPingListener ();
	Pinger.start (true);
	HabboClient.resizeToFitScreenIfNeeded ();
</script >
<div id="fb-root" ></div >
<script type="text/javascript" >
	window.fbAsyncInit = function () {
		Cookie.erase ("fbsr_1417574575138432");
		FB.init (
			{
				appId: '{{fb_appid}}',
				channelUrl: '/fbchannel',
				status: true,
				cookie: true,
				xfbml: true
			}
		);
		FB.getLoginStatus (
			function (oSession) {
				if (typeof comufy_storeUser != 'undefined') {
					if (oSession.status !== 'connected') {
						FB.Event.subscribe (
							'auth.login', function (oSession) {
								if (oSession.status == "connected") {
									comufy_storeUser (oSession, {});
								}
							}
						);
					}
					else {
						comufy_storeUser (oSession, {});
					}
				}
			}
		);
		if (window.habboPageInitQueue) {
			// jquery might not be loaded yet
			habboPageInitQueue.push (
				function () {
					$ (document).trigger ("fbevents:scriptLoaded");
				}
			);
		}
		else {
			$ (document).fire ("fbevents:scriptLoaded");
		}
	};
	window.assistedLogin = function (FBobject, optresponse) {
		Cookie.erase ("fbsr_1417574575138432");
		FBobject.init (
			{
				appId: '{{fb_appid}}',
				channelUrl: '/fbchannel',
				status: true,
				cookie: true,
				xfbml: true
			}
		);
		permissions = 'user_birthday,email,user_likes';
		defaultAction = function (response) {
			if (response.authResponse) {
				fbConnectUrl = "/facebook?connect=true
				Cookie.erase ("fbhb_val_1417574575138432");
				Cookie.set ("fbhb_val_1417574575138432", response.authResponse.accessToken);
				Cookie.erase ("fbhb_expr_1417574575138432");
				Cookie.set ("fbhb_expr_1417574575138432", response.authResponse.expiresIn);
				window.location.replace (fbConnectUrl);
			}
		};
		if (typeof optresponse == 'undefined')
			FBobject.login (defaultAction, {scope: permissions});
		else
			FBobject.login (optresponse, {scope: permissions});
	};
	(function () {
		var e = document.createElement ('script');
		e.async = true;
		e.src = 'https://connect.facebook.net/en_US/all.js';
		document.getElementById ('fb-root').appendChild (e);
	} ());
</script >
<div id="FB_HiddenIFrameContainer"
     style="display:none; position:absolute; left:-100px; top:-100px; width:0px; height: 0px;" ></div >
<script type="text/javascript" >
	FacebookIntegration.apiKey = "{{fb_int_id}}
	FacebookIntegration.applicationId = "{{user_id}}843{{user_id}}000
	FacebookIntegration.applicationName = "{{fb_int_name}}
	FacebookIntegration.badgeImagePath = "{{hotel_url}}habbo-imaging/decorate/001
	FacebookIntegration.viralPresentImagePath = "{{hotel_url}}habbo-imaging/decorate/005
	FacebookIntegration.viralPartnerCode = "FBVIR
	FacebookIntegration.fbAppRequestUserFilter = "all
	L10N.put ("facebook.story.actionlink.text", "get the Reward");
	L10N.put ("facebook.story.name", "Breaking News From Habbo Hotel!");
	L10N.put ("facebook.story.registration.name", "Welcome to Habbo Hotel");
	L10N.put ("facebook.story.registration.description", "Starting a new life as a Habbo in Habbo Hotel.");
	L10N.put ("facebook.story.registration.prompt", "You are a Habbo now. Whaddya think?");
	L10N.put ("facebook.story.achievement.prompt", "add a message to your friends");
	L10N.put ("facebook.story.registration.caption", "{0} just checked into the Hotel!");
	L10N.put ("facebook.story.achievement.caption", "{0} needs your help to get an achievement reward!");
	L10N.put ("facebook.story.xmasviral.actionlink.text", "open the package");
	L10N.put ("facebook.story.xmasviral.prompt", "Tell your friends you need their help!");
	L10N.put ("facebook.request.content.text", "I found a mystery package in Habbo! Please help me open it!");
	L10N.put ("title.fb_app_request", "Send Ribbit Request");
	FacebookIntegration.initUI ();
</script >
<iframe id="conversion-tracking" src="about:blank" width="0" height="0" frameborder="0" scrolling="no" marginwidth="0"
        marginheight="0" style="position: absolute; top:0; left:0" ></iframe >
<script src="//tracker.comufy.com/tracker.js?domain=sulake&appId=1417574575138432&autostore=false"
        type="text/javascript" ></script >
<script type="text/javascript" >

	/* stub firebug console methods in case Firebug is not present */
	if (! ("console" in window) || ! ("firebug" in console)) {
		var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml",
			"group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
		window.console = {};
		for (var i = 0; i < names.length; ++ i)
			window.console[names[i]] = function () {
			}
	}
	var ssa_json = {
		'applicationUserId': '{{user_id}}843{{user_id}}000',
		'applicationKey': '{{sonic_id}}',
		'onCampaignsReady': supersonicAdsOnCampaignsReady,
		'onCampaignOpen': supersonicAdsOnCampaignOpen,
		'onCampaignClose': supersonicAdsOnCampaignClose,
		'onCampaignCompleted': supersonicAdsOnCampaignCompleted,
		'pagination': false,
		'customCss': '{{gallery_url}}styles/supersonicads.css'
	};
	function supersonicAdsCamapaignEngage () {
		console.log ("supersonicAdsCamapaignEngage");
		SSA_CORE.BrandConnect.engage ();
		var topBar = document.getElementById ("ssaInterstitialTopBar");
		var innerHTML = topBar.innerHTML;
		topBar.innerHTML = "
		var topBarInnerContainerLeft = document.createElement ("div");
		topBarInnerContainerLeft.className = "ssaInterstitialTopBarInnerContainerLeft
		var topBarInnerContainerRight = document.createElement ("div");
		topBarInnerContainerRight.className = "ssaInterstitialTopBarInnerContainerRight
		var closeButton = document.createElement ("div");
		closeButton.className = "ssaTopBarCloseButton
		closeButton.setAttribute ("onClick", "SSA_CORE.close('ssaBrandConnect')");
		closeButton.innerHTML = "
		var textDiv = document.createElement ("span");
		textDiv.className = "ssaTopBarTextSpan
		textDiv.innerHTML = innerHTML;
		topBarInnerContainerLeft.appendChild (closeButton);
		topBarInnerContainerLeft.appendChild (textDiv);
		topBar.appendChild (topBarInnerContainerRight);
		topBar.appendChild (topBarInnerContainerLeft);
		var bottomBar = document.getElementById ("ssaInterstitialBottomBar");
		var bottomInnerContainerLeft = document.createElement ("div");
		bottomInnerContainerLeft.className = "ssaBottomBarInnerLeft
		var bottomInnerContainerRight = document.createElement ("div");
		bottomInnerContainerRight.className = "ssaBottomBarInnerRight
		bottomBar.appendChild (bottomInnerContainerRight);
		bottomBar.appendChild (bottomInnerContainerLeft);
	}
	function supersonicAdsOnCampaignsReady (offers) {
		if (typeof offers !== 'undefined' && offers.length) {
			console.log ("supersonicAdsOnCampaignsReady offers: " + offers.length);
			for (var i = 0; i < offers.length; i ++) {
				console.log (offers[i]);
			}
			FlashExternalInterface.clientElement.supersonicAdsOnCampaignsReady (offers.length.toString ());
		}
		else {
			console.log ("supersonicAdsOnCampaignsReady no offers!");
			FlashExternalInterface.clientElement.supersonicAdsOnCampaignsReady ("0");
		}
	}
	function supersonicAdsOnCampaignOpen (offer) {
		console.log ("supersonicAdsOnCampaignOpen");
		console.log (offer);
		FlashExternalInterface.clientElement.supersonicAdsOnCampaignOpen ();
	}
	function supersonicAdsOnCampaignClose (offer) {
		console.log ("supersonicAdsOnCampaignClose");
		console.log (offer);
		FlashExternalInterface.clientElement.supersonicAdsOnCampaignClose ();
	}
	function supersonicAdsOnCampaignCompleted (offer) {
		console.log ("supersonicAdsOnCampaignCompleted");
		console.log (offer);
		FlashExternalInterface.clientElement.supersonicAdsOnCampaignCompleted ();
	}
	function supersonicAdsLoadCampaigns () {
		// We need the client to have wmode=opaque or wmode=transparent for video offers
		if (HabbletLoader.needsFlashKbWorkaround ()) {
			return;
		}
		console.log ("loading supersonic script");
		var g = document.createElement ('script');
		var s = document.getElementsByTagName ('script')[0];
		g.async = true;
		g.src = ("https:" != location.protocol ? "http://jsd.supersonicads.com" : "https://a248.e.akamai.net/ssastatic.s3.amazonaws.com") + "/inlineDelivery/delivery.min.gz.js
		s.parentNode.insertBefore (g, s);
	}
</script >
<script type="text/javascript" >
	var _gaq = _gaq || [];
	_gaq.push (['_setAccount', 'UA-448325-2']);
	_gaq.push (['_trackPageview']);
	window.setTimeout ("_gaq.push(['_setCustomVar', 5, 'facebook', 'client open', 2])", 2000);
	(function () {
		var ga = document.createElement ('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName ('script')[0];
		s.parentNode.insertBefore (ga, s);
	}) ();
</script >
<script type="text/javascript" >
	HabboView.run ();
</script >
</body >
</html>