<?php

defined ( 'ROOT_PATH' ) || define('ROOT_PATH', realpath ( dirname ( __FILE__ ) . '/../' ));
defined ( 'TIME_GOIN' ) || define('TIME_GOIN', microtime ( true ));

/*
 * * Azure CMS
 * * The Azure Project
 * * Maked by Claudio Santoro (bi0s)
 * * fb.com/sant0ros twitter: @m0vame skype: live:sant0ro
 */

/*
 * * Start the Session
 */

session_start ();

/*
 * * Settings of CMS
 */

$database_settings = array ('host' => 'localhost', 'user' => 'root', 'pass' => '', 'name' => 'azure', 'port' => 3306, 'type' => 'mysql');
$hotel_settings    = array ('hotel_name' => 'azure', 'hotel_url' => clink_to ( '' ), 'gallery_url' => '/public/web-gallery/', 'swf_url' => '/public/', 'client_name' => '/client/', 'comb_name' => '1.xml', 'use_comb' => TRUE, 'emu_ip' => '127.0.0.1', 'emu_port' => '300', 'safe_emu' => FALSE, 'swf_name' => 'habbo.swf', 'gordon_path' => 'RELEASE63-201411201226-580134750/', 'fb_appid' => '1417574575138432', 'fb_secret' => '', 'sonic_id' => '2abb40ad', 'fb_int_name' => 'habboen', 'fb_int_id' => '65d5e60e738877cb53bb5004edf6a8fc', 'twitter_on' => 'visible', 'twitter_name' => 'habbo', 'twitter_id' => '345163210678206465', 'paypalurl' => 'www.paypal.com/order/4535535/');
$swf_combo_values  = array ('variables' => '1.xml', 'texts' => '1.xml', 'figuredata' => '1.xml', 'furnidata' => '1.xml', 'productdata' => '1.xml', 'o_variables' => '1.xml', 'o_texts' => '1.xml');
$subheader_array   = array ('pages' => array ('me' => 'Home', 'staff' => 'Staff', 'safety' => 'Safety', 'credits' => 'Credits', 'donate' => 'Donate', 'rares' => 'Rares'), 'subpages' => array ('logout' => 'Sign Out'));

/*
 * * Classes
 */

final
class Hotel
{
	
	var $online_users     = 0;
	var $registered_users = 0;
	var $server_ver       = '1.0';
	
	public
	function __construct ( $online_user, $server_ver, $registered_users )
	{
		$this->online_users     = $online_user;
		$this->registered_users = $registered_users;
		$this->server_ver       = $server_ver;
	}
	
	public
	function get_hotel ( $Var )
	{
		return $this->$Var;
	}
	
}

final
class Habbo
{
	
	var $user_id;
	var $user_name;
	var $user_email;
	var $user_gender;
	var $user_motto;
	var $user_credits;
	var $user_duckets;
	var $ip_address;
	var $client_ticket;
	var $user_look;
	var $user_is_admin;
	
	public
	function __construct ( $user_id = 0, $user_name = '', $user_email = '', $user_gender = 'M', $user_motto = 'default motto', $user_credits = 0, $user_duckets = 0, $user_ip = '', $client_ticket = '', $user_look = '', $user_is_admin = false )
	{
		$this->user_id       = $user_id;
		$this->user_name     = $user_name;
		$this->user_email    = $user_email;
		$this->user_gender   = $user_gender;
		$this->user_motto    = $user_motto;
		$this->user_credits  = $user_credits;
		$this->user_duckets  = $user_duckets;
		$this->ip_address    = $user_ip;
		$this->client_ticket = $client_ticket;
		$this->user_look     = $user_look;
		$this->user_is_admin = $user_is_admin;
	}
	
	public static
	function client_ticket ()
	{
		$data = rand ( 0, 8 );
		for ($i = 1; $i <= 9; $i++)
			$data .= rand ( 0, 8 );
		$data .= "d";
		$data .= rand ( 0, 4 );
		$data .= "c";
		$data .= rand ( 0, 6 );
		$data .= "c";
		$data .= rand ( 0, 8 );
		$data .= "c";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 4 );
		$data .= "d";
		for ($i = 1; $i <= 3; $i++)
			$data .= rand ( 0, 6 );
		$data .= "ae";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 6 );
		$data .= "bcb";
		$data .= rand ( 0, 4 );
		$data .= "a";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 8 );
		$data .= "c";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 4 );
		$data .= "a";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 8 );
		return $data;
	}

	public static
	function user_hash ()
	{
		$data = rand ( 0, 9 );
		for ($i = 1; $i <= 9; $i++)
			$data .= rand ( 0, 9 );
		$data .= "d";
		$data .= rand ( 0, 7 );
		$data .= "c";
		$data .= rand ( 0, 5 );
		$data .= "c";
		$data .= rand ( 0, 3 );
		$data .= "c";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 5 );
		$data .= "d";
		for ($i = 1; $i <= 3; $i++)
			$data .= rand ( 0, 7 );
		$data .= "ae";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 7 );
		$data .= "bcb";
		$data .= rand ( 0, 5 );
		$data .= "a";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 9 );
		$data .= "c";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 3 );
		$data .= "a";
		for ($i = 1; $i <= 2; $i++)
			$data .= rand ( 0, 9 );
		return $data;
	}

	public
	function get_habbo ( $var )
	{
		return $this->$var;
	}

	public
	function window_name ()
	{
		$data = rand ( 0, 9 );
		for ($i = 1; $i <= 29; $i++)
			$data .= rand ( 0, 9 );
		return $data;
	}

}

final
class DatabaseConnector
{

	private
	function __construct ()
	{

	}

	public static
	function open ( $db )
	{

		if (isset($db))
		{
			$user = isset($db['user']) ? $db['user'] : NULL;
			$pass = isset($db['pass']) ? $db['pass'] : NULL;
			$name = isset($db['name']) ? $db['name'] : NULL;
			$host = isset($db['host']) ? $db['host'] : NULL;
			$type = isset($db['type']) ? $db['type'] : NULL;
			$port = isset($db['port']) ? $db['port'] : NULL;

			switch ($type)
			{
				case 'pgsql':
					$port = $port ? $port : '5432';
					$conn = new PDO( "pgsql:dbname={$name}; user={$user}; password={$pass};
                        host=$host;port={$port}" );
					break;
				case 'mysql':
					$port = $port ? $port : '3306';
					if ($pass != NULL)
						$conn = new PDO( "mysql:host={$host};port={$port};dbname={$name}", $user, $pass );
					else
						$conn = new PDO( "mysql:host={$host};port={$port};dbname={$name}", $user );
					break;
				case 'sqlite':
					$conn = new PDO( "sqlite:{$name}" );
					break;
				case 'ibase':
					$conn = new PDO( "firebird:dbname={$name}", $user, $pass );
					break;
				case 'oci8':
					$conn = new PDO( "oci:dbname={$name}", $user, $pass );
					break;
				case 'mssql':
					$conn = new PDO( "mssql:host={$host},1433;dbname={$name}", $user, $pass );
					break;
			}
		}

		$conn->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		return $conn;
	}

}

final
class DatabaseAdapter
{

	private static $conn_handler;

	private
	function __construct ()
	{

	}

	public static
	function open ( $database )
	{
		if (empty(self::$conn_handler)):
			self::$conn_handler = DatabaseConnector::open ( $database );
			return true;
		endif;
	}

	public static
	function get ()
	{
		return self::$conn_handler;
	}

	public static
	function close ()
	{
		if (self::$conn_handler):
			self::$conn_handler = NULL;
		endif;
	}

	public static
	function fetch_all ( $query_row = NULL )
	{

		try
		{
			if (isset($query_row)):
				if ($query_row != "not_started"):
					$row = $query_row->fetch ( PDO::FETCH_ASSOC );
					return $row;
				else:
					echo xEcho ( "Exception on Violation database: database not Started" );
				endif;
			else:
				return "";
			endif;
		}
		catch (PDOException $exception)
		{
			echo xEcho ( "Exception on Violation database: " . $exception );
		}
	}

	public static
	function insert_array ( $table, $data, $exclude = [] )
	{
		$fields = $values = array ();
		if (!is_array ( $exclude ))
			$exclude = array ($exclude);
		foreach (array_keys ( $data ) as $key)
		{
			if (!in_array ( $key, $exclude )):
				$fields[] = "`$key`";
				$values[] = "'" . htmlentities ( addslashes ( $data[$key] ) ) . "'";
			endif;
		}
		$fields = implode ( ",", $fields );
		$values = implode ( ",", $values );
		DatabaseAdapter::query ( "INSERT INTO `$table` ($fields) VALUES ($values)" );
	}

	public static
	function query ( $query = NULL )
	{
		try
		{
			if (isset(self::$conn_handler)):
				$query_result = self::$conn_handler->query ( $query );
				if (isset($query_result)):
					return $query_result;
				endif;
			else:
				echo xEcho ( "Exception on Violation database: database not Started" );
				return "not_started";
			endif;
		}
		catch (PDOException $exception)
		{
			echo xEcho ( "Exception on Violation database: " . $exception );
		}
	}

	public static
	function num_rows ( $query = NULL )
	{
		try
		{
			if (isset($query)):
				if ($query != "not_started"):
					$count_data = $query->rowCount ();
					return $count_data;
				else:
					echo xEcho ( "Exception on Violation database: database not Started" );
				endif;
			else:
				return "";
			endif;
		}
		catch (PDOException $exception)
		{
			echo xEcho ( "Exception on Violation database: " . $exception );
		}
	}

}

class Script
{

	var $scripts;

	public
	function __construct ()
	{
		$this->scripts = '';
	}

	public
	function add ( $url = '' )
	{
		$url = link_to ( $url );
		$this->scripts .= "<script src='$url' type='text/javascript'></script>" . "\n\r";
	}

	public
	function get ()
	{
		return $this->scripts;
	}

}

class ScriptCode
{

	var $scripts_code;

	public
	function __construct ()
	{
		$this->scripts_code = "<script type='text/javascript'>" . "\n\r";
	}

	public
	function add ( $code = '' )
	{
		$this->scripts_code .= $code;
	}

	public
	function get ()
	{
		$this->scripts_code .= "</script>";
		return $this->scripts_code;
	}

}

class Styles
{

	var $styles;

	public
	function __construct ()
	{
		$this->styles = '';
	}

	public
	function add ( $url = '' )
	{
		$url = link_to ( $url );
		$this->styles .= "<link rel='stylesheet' href='$url' type='text/css'/>" . "\n\r";
	}

	public
	function get ()
	{
		return $this->styles;
	}

}

class Header
{

	var $scripts;
	var $styles;
	var $content;

	public
	function __construct ( $web_gallery_url, $page_type = 'default' )
	{
		$this->scripts = new Script();
		$this->styles  = new Styles();
		switch ($page_type)
		{
			case 'client':
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/common.js" );
				$this->styles->add ( $web_gallery_url . "static/styles/habboflashclient.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				break;
			case 'fproblem':
				$this->styles->add ( $web_gallery_url . "v2/styles/process.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/habboflashclient.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/v3_habblet.css" );
				break;
			case 'me':
				$this->scripts->add ( $web_gallery_url . "static/js/lightweightmepage.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				$this->styles->add ( $web_gallery_url . "static/styles/lightweightmepage.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				break;
			case 'credits':
			case 'rares':
				$this->scripts->add ( $web_gallery_url . "static/js/lightweightmepage.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				$this->styles->add ( $web_gallery_url . "static/styles/lightweightmepage.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/newcredits.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				break;
			case 'staff':
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				break;
			case 'index':
				$this->scripts->add ( $web_gallery_url . "static/js/libs2.js" );
				$this->styles->add ( $web_gallery_url . "v2/styles/process.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				break;
			case 'register':
				$this->scripts->add ( $web_gallery_url . "static/js/libs2.js" );
				$this->styles->add ( $web_gallery_url . "v2/styles/process.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/buttons.css" );
				break;
			case 'createnews':
				$this->styles->add ( $web_gallery_url . "static/styles/lightweightmepage.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/process.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/buttons.css" );
				break;
			case 'safety':
				$this->styles->add ( $web_gallery_url . "static/styles/safety.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/buttons.css" );
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				break;
			case 'welcome':
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/process.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/buttons.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/welcome.css" );
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				break;
			default:
				$this->styles->add ( $web_gallery_url . "static/styles/common.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/process.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/style.css" );
				$this->styles->add ( $web_gallery_url . "v2/styles/buttons.css" );
				$this->scripts->add ( $web_gallery_url . "static/js/visual.js" );
				$this->scripts->add ( $web_gallery_url . "static/js/libs.js" );
				break;
		}
		return;
	}

	public
	function get ()
	{
		$C = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n";
		$C .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
		$C .= "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
		$C .= "<head>\n";
		$C .= "<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\" />\n";
		$C .= "<title>azure</title>\n";
		$C .= $this->scripts->get ();
		$C .= $this->styles->get ();
		$C .= "</head>\n";
		return $C;
	}

}

class Tags
{

	protected $children;          // nome da TAG
	private   $name;    // propriedades da TAG
	private   $properties;

	public
	function __construct ( $name )
	{

		$this->name = $name;
	}

	public
	function __set ( $name, $value )
	{

		$this->properties[$name] = $value;
	}

	public
	function add ( $child )
	{
		$this->children[] = $child;
	}

	public
	function show ()
	{
		$Code = '';
		$Code .= $this->open ();
		$Code .= "\n";

		if ($this->children)
		{

			foreach ($this->children as $child)
			{

				if (is_object ( $child ))
				{
					$Code .= $child->show ();
				}
				else if ((is_string ( $child )) or (is_numeric ( $child )))
				{

					$Code .= $child;
				}
			}

			$Code .= $this->close ();
		}
		return $Code;
	}

	private
	function open ()
	{
		$Code = '';
		$Code .= "<{$this->name}";
		if ($this->properties)
		{
			foreach ($this->properties as $name => $value)
			{
				$Code .= " {$name}=\"{$value}\"";
			}
		}
		$Code .= '>';
		return $Code;
	}

	private
	function close ()
	{
		$Code = '';
		$Code .= "</{$this->name}>\n";
		return $Code;
	}

}

class Content
{

	var $Body;
	var $Content;
	var $Container;

	public static
	function habblet_container ( $Type, $Content, $Title = '' )
	{
		$Tag         = new Tags( 'div' );
		$Tag->class  = 'habblet-container';
		$Tag2        = new Tags( 'div' );
		$Tag2->class = "cbb clearfix $Type";
		$Tag->add ( $Tag2 );
		if (!empty($Title)):
			$Tag3        = new Tags( 'h2' );
			$Tag3->class = 'title';
			$Tag3->add ( $Title );
			$Tag2->add ( $Tag3 );
		else:
			$Tag2->add ( '' );
		endif;
		$Tag4        = new Tags( 'div' );
		$Tag4->class = 'box-content';
		$Tag4->add ( $Content );
		$Tag2->add ( $Tag4 );
		return $Tag;
	}

	public
	function start_body ( $PageId )
	{
		$PageIdClass = '';
		$Style       = '';
		$PageIdType  = '';

		switch ($PageId)
		{
			case 'me':
			case 'welcome':
			case 'staff':
			case 'safety':
				$PageIdType = 'home';
				$Style      = 'width: 100%;padding: 0;background: #eee !important;';
				break;
			case 'credits':
				$PageIdType = 'newcredits';
				break;
			case 'fproblem':
				$PageIdType  = 'popup';
				$PageIdClass = 'process-template client_error';
				break;
			case 'articles':
				$PageIdType = 'news';
				break;
			case 'index':
			case 'register':
			case 'createnews':
				$PageIdClass = 'process-template';
				$Style       = 'width: 100%;padding:0;background-color:rgb(241, 241, 241);color:rgb(50, 50, 50);';
				break;
			case 'client':
				$PageIdType = 'client';
				break;
			default:
				$PageIdType = 'home';
				break;
		}
		switch ($PageId)
		{
			case 'register':
			case 'createnews':
			case 'index':
				$Body            = new Tags( 'body' );
				$Body->id        = $PageIdType;
				$Body->class     = $PageIdClass;
				$Body->style     = $Style;
				$this->Body      = $Body;
				$Container       = new Tags( 'div' );
				$Container->id   = 'container';
				$this->Body      = $Body;
				$this->Container = $Container;
				break;
			case 'me':
			case 'welcome':
			case 'staff':
			case 'credits':
			case 'rares':
			case 'donate':
			case 'safety':
			case 'articles':
				$Body        = new Tags( 'body' );
				$Body->id    = $PageIdType;
				$Body->class = $PageIdClass;
				$Body->style = $Style;
				$Code        = '';
				$Code .= "<link href='http://fonts.googleapis.com/css?family=open+Sans:300' rel='stylesheet' type='text/css'>\n";
				$Code .= "<div id=\"header-container\" style=\"background: url(http://foundation.zurb.com/assets/img/marquee-stars.svg) #074e68;height: 110px;\">\n";
				$Code .= "<div id=\"header\" class=\"clearfix\" style=\"background:none;\">\n";
				$Code .= Page::LogoHeader ();
				$Code .= Page::SubHeader ( $PageId );
				$Body->add ( $Code );
				$Container       = new Tags( 'div' );
				$Container->id   = 'container';
				$Content         = new Tags( 'div' );
				$Content->id     = 'content';
				$this->Body      = $Body;
				$this->Container = $Container;
				$this->Content   = $Content;
				break;
			case 'client':
				$this->Body = '';
				break;
			default:
				$Body            = new Tags( 'body' );
				$Body->id        = $PageIdType;
				$Body->class     = $PageIdClass;
				$Body->style     = $Style;
				$Container       = new Tags( 'div' );
				$Container->id   = 'container';
				$Content         = new Tags( 'div' );
				$Content->id     = 'content';
				$this->Body      = $Body;
				$this->Container = $Container;
				$this->Content   = $Content;
				break;
		}
	}

	public
	function add_content ( $Code )
	{
		$this->Body->add ( $Code );
	}

	public
	function end_body ( $Footer = 1 )
	{
		$Code = $this->Body->show ();
		return $Code;
	}

	public
	function generic_body ( $Code = '' )
	{
		if (!empty($this->Container) && !empty($this->Content)):
			$this->Content->add ( $Code );
			$this->Container->add ( $this->Content );
			$this->Body->add ( $this->Container );
		elseif (empty($this->Container) && !empty($this->Content)):
			$this->Content->add ( $Code );
			$this->Body->add ( $this->Content );
		elseif (!empty($this->Container) && empty($this->Content)):
			$this->Container->add ( $Code );
			$this->Body->add ( $this->Container );
		endif;
		if (!empty($this->Body)):
			$Code = $this->Body->show ();
		endif;
		return $Code;
	}

}

class Body
{

	public static
	function page ( $PageId )
	{
		if (!isset($code))
			$code = "";
		$Body = new Content;
		$Body->start_body ( $PageId );
		switch ($PageId)
		{
			case "register":
				$code .= Body::body_overlay ( 1 );
				$code .= render_content ( $PageId );
			case "index":
				if (StorageHabbo::get_habbo ( 'user_id' ) != "not_logged"):
					redirect ( '/me/' );
				endif;
				$code .= render_content ( $PageId );
				break;
			case "createnews":
				if (StorageHabbo::get_habbo ( 'user_is_admin' )):
					$code .= Body::body_overlay ( 1 );
					$code .= render_content ( $PageId );
				endif;
			case "me":
				$code .= render_content ( $PageId );
				break;
			case "rares":
				$code .= render_content ( $PageId );
				break;
			case "articles":
				$code .= render_content ( $PageId );
				break;
			case "donate":
				$code .= render_content ( $PageId );
				break;
			case "welcome":
				$code .= render_content ( $PageId );
				break;
			case "staff":
				$code .= render_content ( $PageId );
				break;
			case "credits":
				$code .= render_content ( $PageId );
				break;
			case "safety":
				$code .= render_content ( $PageId );
				break;
			case "client":
				if (!StorageHabbo::$NeedDatabase):
					StorageHabbo::$NeedDatabase = true;
				endif;
				$code .= render_content ( $PageId );
				break;
		}
		$codes = $Body->generic_body ( $code );
		return $codes;
	}

	public static
	function body_overlay ( $Enabled = 0 )
	{
		$Tag     = new Tags( 'div' );
		$Tag->id = 'overlay';
		if ($Enabled):
			$Tag->style = 'display:block;height:100%;z-index:9000;position:fixed;';
		endif;
		$Tag->add ( '' );
		$Tag = $Tag->show ();
		return $Tag;
	}

}

class Footer
{

	public
	function __construct ()
	{

	}

}

class Page
{

	static $ForceDatabase = false;
	var    $Settings;
	var    $Hotel;
	var    $Habbo;

	public
	function __construct ( $Settings, $Hotel, $Habbo )
	{
		$this->Settings = $Settings;
		$this->Hotel    = $Hotel;
		$this->Habbo    = $Habbo;
	}

	public static
	function Footer ()
	{
		$Code = '';
		$Code .= "<div id=\"footer\">\n";
		$Code .= "<p class=\"footer-links\">{{hotel_name}} Is A Private habbo Retro Server -  <a href=\"#\" target=\"_new\">Sulake</a> l <a href=\"#\" target=\"_new\">Terms of Use</a> l <a href=\"#\" target=\"_new\">Privacy Policy</a> l <a href=\"#\" target=\"_new\">Infringements</a> l <a href=\"#\" target=\"_new\"> Terms of Sale - US</a> l <a href=\"#\" target=\"_new\">The habbo Way</a> l <a href=\"#\">Safety Tips</a> l <a href=\"#\">Parents</a> l <a href=\"mailto: advertising@sulake.com?\" taget=\"_new\">Advertise With Us</a></p>\n";
		$Code .= "<p class=\"copyright\">© 2004 - 2014 Sulake Corporation Oy. HABBO is a registered trademark of Sulake Corporation Oy. All rights reserved. {{hotel_name}} Isn't Original habbo Server!</p>\n";
		$Code .= '</div>';
		return $Code;
	}

	public static
	function SubHeader ( $PageName = "me" )
	{
		if (Page::sub_header_started () == "code_ok"):
			$Code      = '';
			$SubHeader = $_SESSION['HotelSubHeader'];
			$Code .= "<div id=\"subnavi\" class=\"narrow\" style=\"left: 694px;width: 66px;\">\n";
			$Code .= "<div id=\"subnavi-search\">\n";
			$Code .= "<div id=\"subnavi-search-upper\">\n";
			$Code .= "<ul id=\"subnavi-search-links\">             \n";
			foreach ($SubHeader['subpages'] as $Key => $Value)
				$Code .= "<li><a href=\"{{hotel_url}}/action/$Key/\" class=\"link\"><span>$Value</span></a></li>\n";
			$Code .= "</ul>\n";
			$Code .= "</div>\n";
			$Code .= "</div>\n";
			$Code .= "</div>\n";
			$Code .= "<ul id=\"navi\">\n";
			$Code .= "</ul>\n";
			$Code .= "	</div>\n";
			$Code .= "</div>\n";
			$Code .= "<div id=\"content-container\" style=\"padding:0;\">\n";
			$Code .= "<div id=\"navi2-container\" style=\"background: none;width: 100%;\">\n";
			$Code .= "<div id=\"navi2\" style=\"border-bottom: 3px solid rgb(204, 204, 204) !important;height: 35px;background: white !important;padding-top: 5px;\">\n";
			$Code .= "<ul>\n";
			foreach ($SubHeader['pages'] as $Key => $Value)
			{
				if ($Key == $PageName):
					$Code .= "<li class=\"selected\" >$Value</li>\n";
				else:
					$Code .= "<li><a href=\"{{hotel_url}}/$Key/\">$Value</a></li>\n";
				endif;
			}
			$Code .= "</ul>\n";
			$Code .= "</div>\n";
			$Code .= "</div>\n";
			return $Code;
		else:
			return 'not_started';
		endif;
	}

	public static
	function sub_header_started ()
	{
		if (isset($_SESSION['HotelSubHeader'])):
			if ($_SESSION['HotelSubHeader'] != ""):
				return 'code_ok';
			endif;
		else:
			return 'not_started';
		endif;
	}

	public static
	function LogoHeader ()
	{
		$Code = "<h1><a href=\"{{hotel_url}}/me/\" style=\"color: white;font-family: 'open Sans', sans-serif;font-weight: lighter;font-size: 35px;text-decoration: none;background-image: none;text-indent: 0;\">{{hotel_name}}</a></h1>";
		return $Code;
	}

	public
	function set_page ()
	{
		header ( 'Cache-Control: no-cache' );
		header ( 'Pragma: no-cache' );

		//Gets the URL
		$requestURI = explode ( '/', $_SERVER['REQUEST_URI'] );
		$scriptName = explode ( '/', $_SERVER['SCRIPT_NAME'] );

		//Remove extra stuff from url before the index.php
		try
		{
			for ($i = 0; $i < sizeof ( $scriptName ); $i++)
				if (@$requestURI[$i] == @$scriptName[$i]):
					unset($requestURI[$i]);
				endif;
		}
		catch (Exception $e)
		{
			// Nothing to Do
		}

		$command = array_values ( $requestURI );

		//Try: fist the entity name, second the action name. The rest goes as parameter/value/parameter/value...
		try
		{
			if ((isset($command[0])) && ($command[0] != '')):
				$E = htmlentities ( addslashes ( strtolower ( $command[0] ) ) );
				if ((isset($command[1])) && ($command[1] != '') && (strpos ( $command[1], "?" ) === false) && (strpos ( $command[1], "&" ) === false)):
					$D = htmlentities ( addslashes ( strtolower ( $command[1] ) ) );
					$F = ['E' => $E, 'D' => $D];
				elseif ($command[0] == 'action'):
					$D = htmlentities ( addslashes ( strtolower ( $command[1] ) ) );
					$F = ['E' => $E, 'D' => $D];
				else:
					$F = ['E' => $E, 'D' => ''];
				endif;
			else:
				$F = ['E' => 'index', 'D' => ''];
			endif;

			if (strpos ( $command[sizeof ( $command ) - 1], "?" ) !== false):
				$c          = explode ( "?", $command[sizeof ( $command ) - 1] );
				$commands[] = $c[0];
				unset($c[0]);
				$commands[] = implode ( "&", $c );
			endif;

			unset($command[0]);
			unset($command[1]);
			if (isset($commands) && is_array ( $commands )):
				foreach ($commands as $key => $value):
					if ($key % 2 == 0):
						$_GET[$value] = "";
						$last         = $value;
					else:
						$_GET[$last] = urldecode ( $value );
					endif;
				endforeach;
			endif;
			return $F;
		}
		catch (Exception $e)
		{
			// Nothing to Do
		}
	}

	public
	function make_page ( $V, $C )
	{
		$Header = new Header( $this->Hotel['gallery_url'], $V );
		$A      = $Header->get ();
		if (!empty($V)):
			if (!empty($C)):
				if (!StorageHabbo::$NeedDatabase):
					StorageHabbo::$NeedDatabase = true;
				endif;
				$C = Actions::set_action ( $C );
				$B = Body::page ( $V );
			elseif ($V == 'action'):
				if (!StorageHabbo::$NeedDatabase):
					StorageHabbo::$NeedDatabase = true;
				endif;
				$C = Actions::set_action ( $C );
				$B = '';
			else:
				$C = '';
				$B = Body::page ( $V );
			endif;
		else:
			$B = '';
		endif;

		return array ('A' => $A, 'B' => $B, 'C' => $C);
	}

	public
	function database ( $Settings )
	{
		if ((StorageHabbo::database_is_available () && (StorageHabbo::database_is_available ( 1 ) == false)) || self::$ForceDatabase == true):
			if (DatabaseAdapter::open ( $Settings )):
				if (!StorageHabbo::check_if_promo_exists ()):
					StorageHabbo::promos ( 0 );
				endif;
				if (!StorageHabbo::check_if_staff_exists ()):
					StorageHabbo::staffs ( 0 );
				endif;
				return true;
			else:
				return false;
			endif;
		else:
			return 'not_loggged';
		endif;
	}

	public
	function start_sub_header ( $SubHeader = array () )
	{
		if (isset($_SESSION['HotelSubHeader'])):
			if ($_SESSION['HotelSubHeader'] != ""):
				return 'code_ok';
			endif;
		else:
			$_SESSION['HotelSubHeader'] = $SubHeader;
			return 'not_started';
		endif;
	}

	public
	function serialize ( $Make )
	{
		$HeaderSerialize = $Make['A'];
		$WaitSerialize   = $Make['B'];
		if (!empty($Make['C'])):
			Actions::execute_action ( $Make['C'] );
		endif;
		if (isset($_POST['HotelMessage'])):
			$MessagePost = $_POST['HotelMessage'];
			if (is_array ( $MessagePost )):
				if (isset($MessagePost['Info'])):
					$InfoMessage = ($MessagePost['Info'] != "") ? $MessagePost['Info'] : "";
					$InfoMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 8px 8px;border-radius: 3px;font-size: 12px;width: 747px !important;\">$InfoMessage</div>";
					if (strpos ( $WaitSerialize, '{{InfoMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{InfoMessage}}', $InfoMessage, $WaitSerialize );
					endif;
				else:
					if (strpos ( $WaitSerialize, '{{InfoMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{InfoMessage}}', '', $WaitSerialize );
					endif;
				endif;
				if (isset($MessagePost['Error'])):
					$ErrorMessage = ($MessagePost['Error'] != "") ? $MessagePost['Error'] : "";
					$ErrorMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 8px 8px;border-radius: 3px;font-size: 12px;width: 747px !important;\">$ErrorMessage</div>";
					if (strpos ( $WaitSerialize, '{{ErrorMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{ErrorMessage}}', $ErrorMessage, $WaitSerialize );
					endif;
				else:
					if (strpos ( $WaitSerialize, '{{ErrorMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{ErrorMessage}}', '', $WaitSerialize );
					endif;
				endif;
				if (isset($MessagePost['Alert'])):
					$AlertMessage = ($MessagePost['Alert'] != "") ? $MessagePost['Alert'] : "";
					$AlertMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 8px 8px;border-radius: 3px;font-size: 12px;width: 747px !important;\">$AlertMessage</div>";
					if (strpos ( $WaitSerialize, '{{AlertMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{AlertMessage}}', $AlertMessage, $WaitSerialize );
					endif;
				else:
					if (strpos ( $WaitSerialize, '{{AlertMessagesMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{AlertMessageMessage}}', '', $WaitSerialize );
					endif;
				endif;
				if (isset($MessagePost['Success'])):
					$SuccessMessage = ($MessagePost['Success'] != "") ? $MessagePost['Success'] : "";
					$SuccessMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 8px 8px;border-radius: 3px;font-size: 12px;width: 747px !important;\">$SuccessMessage</div>";
					if (strpos ( $WaitSerialize, '{{SuccessMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{SuccessMessage}}', $SuccessMessage, $WaitSerialize );
					endif;
				else:
					if (strpos ( $WaitSerialize, '{{SuccessMessage}}' ) != 0):
						$WaitSerialize = str_replace ( '{{SuccessMessage}}', '', $WaitSerialize );
					endif;
				endif;
			endif;
		else:
			if (strpos ( $WaitSerialize, '{{SuccessMessage}}' ) != 0):
				$WaitSerialize = str_replace ( '{{SuccessMessage}}', '', $WaitSerialize );
			endif;
			if (strpos ( $WaitSerialize, '{{AlertMessagesMessage}}' ) != 0):
				$WaitSerialize = str_replace ( '{{AlertMessageMessage}}', '', $WaitSerialize );
			endif;
			if (strpos ( $WaitSerialize, '{{ErrorMessage}}' ) != 0):
				$WaitSerialize = str_replace ( '{{ErrorMessage}}', '', $WaitSerialize );
			endif;
			if (strpos ( $WaitSerialize, '{{InfoMessage}}' ) != 0):
				$WaitSerialize = str_replace ( '{{InfoMessage}}', '', $WaitSerialize );
			endif;
		endif;
		if (StorageHabbo::check_if_habbo_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			try
			{
				foreach ($this->Habbo as $Key => $Value)
				{
					$Key    = strtolower ( $Key );
					$String = '{{' . $Key . '}}';
					if (strpos ( $WaitSerialize, $String ) != 0):
						$WaitSerialize = str_replace ( $String, $Value, $WaitSerialize );
					else:
						$WaitSerialize = $WaitSerialize;
					endif;
				}
			}
			catch (Exception $Ex)
			{

			}
		endif;
		foreach ($this->Hotel as $Key => $Value)
		{
			if (($Key == 'gallery_url') || ($Key == 'swf_url'))
				$Value = link_to ( $Value );
			$String = '{{' . $Key . '}}';
			if (strpos ( $WaitSerialize, $String ) != 0):
				$WaitSerialize = str_replace ( $String, $Value, $WaitSerialize );
			else:
				$WaitSerialize = $WaitSerialize;
			endif;
		}
		if (!StorageHabbo::check_if_hotel_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (StorageHabbo::database_is_available ( 1 )):
				$Hotel = StorageHabbo::hotel ( 1 );
				foreach ($Hotel as $Key => $Value)
				{
					$String = '{{' . $Key . '}}';
					if (strpos ( $WaitSerialize, $String ) != 0):
						$WaitSerialize = str_replace ( $String, $Value, $WaitSerialize );
					else:
						$WaitSerialize = $WaitSerialize;
					endif;
				}
			endif;
		else:
			$HotelData = StorageHabbo::get_hotel ();
			foreach ($HotelData as $Key => $Value)
			{
				$Key    = strtolower ( $Key );
				$String = '{{' . $Key . '}}';
				if (strpos ( $WaitSerialize, $String ) != 0):
					$WaitSerialize = str_replace ( $String, $Value, $WaitSerialize );
				else:
					$WaitSerialize = $WaitSerialize;
				endif;
			}
		endif;
		if (strpos ( $WaitSerialize, '{{client_ticket}}' ) != 0):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (!StorageHabbo::database_is_available ( 1 )):
				echo xEcho ( "Exception on Violation database: Getting client_ticket" );
			else:
				$Ticket        = Habbo::client_ticket ();
				$WaitSerialize = str_replace ( '{{client_ticket}}', $Ticket, $WaitSerialize );
				$UserId        = StorageHabbo::get_habbo ( 'user_id' );
				DatabaseAdapter::query ( "UPDATE users SET auth_ticket = '$Ticket' WHERE id = $UserId" );
			endif;
		endif;
		if (strpos ( $WaitSerialize, '{{user_hash}}' ) != 0):
			$WaitSerialize = str_replace ( '{{user_hash}}', Habbo::user_hash (), $WaitSerialize );
		endif;
		$FinalSerialize = $HeaderSerialize . $WaitSerialize;
		return $FinalSerialize;
	}

}

final
class Actions
{

	private static $Action;

	public static
	function set_action ( $C )
	{
		return $C;
	}

	public static
	function execute_action ( $Action )
	{
		Actions::prepare_action ( $Action );
	}

	private static
	function prepare_action ( $Action )
	{
		switch ($Action)
		{
			case "clientlog":
				if (isset($_POST["flashStep"]))
					Actions::edit_log ( date ( "F j, Y, g:i a" ) . "[" . $_POST["flashStep"] . "]" );
				break;
			case "ping":
				header ( 'X-JSON: {"privilegeLevel":"1"}' );
				header ( 'X-Origin-Id: resin-fe-3' );
				break;
			case "cache":
				header ( 'X-JSON: {"result":"true"}' );
				header ( 'X-Origin-Id: resin-fe-3' );
				break;
			case "register":
				foreach ($_POST as $Key => $Value)
					$Register[$Key] = $Value;
				StorageHabbo::register_user ( $Register );
				break;
			case "login":
				StorageHabbo::user_login ( $_POST['username'], $_POST['password'] );
				break;
			case "logout":
				if (isset($_SESSION)):
					session_destroy ();
				else:
					session_start ();
					session_destroy ();
				endif;
				StorageHabbo::destroy ();
				redirect ( '/index/' );
				break;
			case "postarticle":
				foreach ($_POST as $Key => $Value)
					$Article[$Key] = $Value;
				StorageHabbo::add_news ( $Article );
				break;
			case "deletenews":
				$ArticleId = htmlentities ( addslashes ( $_GET['deleteid'] ) );
				if (is_numeric ( $ArticleId )):
					StorageHabbo::delete_news ( $ArticleId );
				else:
					echo xEcho ( "Invalid Article Id to Delete.." );
				endif;
				break;
			case "promo":
				echo "<?
                xml version = \"1.0\" encoding=\"UTF-8\"?>\n";
				echo "<habbos>\n";
				$Promos = StorageHabbo::promos ();
				$Nums   = $_SESSION['PromoNums'];
				for ($Row = 1; $Row <= $Nums; $Row++)
					printf ( "<habbo id=\"%s\" name=\"%s\" motto=\"%s\" url=\"user_profile.php?name=%s\" image=\"http://www.habbo.de/habbo-imaging/avatarimage?figure=%s&size=b&direction=4&head_direction=3&gesture=sml\" badge=\"%s\" status=\"%s\" %s />\n", $Promos[$Row]['id'], $Promos[$Row]['username'], stripslashes ( $Promos[$Row]['motto'] ), $Promos[$Row]['username'], $Promos[$Row]['look'], '', 1, '' );
				echo "</habbos>";
				break;
		}
	}

	private static
	function edit_log ( $Data )
	{
		$fp = fopen ( "logs/" . str_replace ( "::1", "127.0.0.1", $_SERVER['REMOTE_ADDR'] ) . ".txt", "a" );
		fwrite ( $fp, $Data . "" );
		fclose ( $fp );
	}

}

final
class StorageHabbo
{

	public static $Habbo;
	public static $Hotel;
	public static $NeedDatabase;

	/* Section user_login and register_user */

	public static
	function register_user ( $Array )
	{
		if (!StorageHabbo::$NeedDatabase):
			StorageHabbo::$NeedDatabase = true;
		endif;
		if (!StorageHabbo::database_is_available ( 1 )):
			echo xEcho ( "Exception on Violation database: function register_user" );
		else:
			if (!StorageHabbo::check_if_habbo_exists ()):
				$Username = strip_tags ( htmlentities ( addslashes ( $Array['username'] ) ) );
				$Password = strip_tags ( htmlentities ( addslashes ( $Array['password'] ) ) );
				$Mail     = htmlentities ( addslashes ( $Array['mail'] ) );
				if (strlen ( $Password ) > 5 && strlen ( $Password ) < 20):
					if (preg_match ( '`[a-z]`', $Password )):
						if (preg_match ( '`[0-9]`', $Password )):
							if (substr_count ( $Password, ' ' ) == 0):
								if (substr_count ( $Username, ' ' ) == 0):
									if (strlen ( $Username ) >= 3):
										if (mb_strlen ( $Username ) < 25):
											if (preg_match ( '`[a-z]`', $Username )):
												$Query = DatabaseAdapter::query ( "SELECT * FROM users WHERE username = \"$Username\" OR mail = \"$Mail\" LIMIT 1" );
												if (DatabaseAdapter::num_rows ( $Query ) == 0):
													$Array['password'] = md5 ( $Password );
													DatabaseAdapter::insert_array ( 'users', $Array );
													StorageHabbo::user_login ( $Username, $Password, 1 );
												else:
													$_POST['HotelMessage'] = array ('Error' => 'A Account with this Username Already Exists');
												endif;
											else:
												$_POST['HotelMessage'] = array ('Error' => 'Your Username Must Contains Letters');
											endif;
										else:
											$_POST['HotelMessage'] = array ('Error' => 'Your Username Can\'t Have more Than 25 Letters');
										endif;
									else:
										$_POST['HotelMessage'] = array ('Error' => 'Your Username Must Have more Than 5 Letters');
									endif;
								else:
									$_POST['HotelMessage'] = array ('Error' => 'Your Username Can\'t Have Spaces');
								endif;
							else:
								$_POST['HotelMessage'] = array ('Error' => 'Your Password Can\'t Have Spaces');
							endif;
						else:
							$_POST['HotelMessage'] = array ('Error' => 'Your Password Must Have Numbers');
						endif;
					else:
						$_POST['HotelMessage'] = array ('Error' => 'Your Password Must Have Letters');
					endif;
				else:
					$_POST['HotelMessage'] = array ('Error' => 'Your Password Must Have More Than 5 Letters And Must Have Maximum of 20 Letters');
				endif;
			else:
				redirect ( '/me/' );
			endif;
		endif;
	}

	public static
	function database_is_available ( $IsIsset = 0 )
	{
		if ($IsIsset == 1):
			if (DatabaseAdapter::get () != null):
				return true;
			else:
				return false;
			endif;
		else:
			if (self::$NeedDatabase == true):
				self::$NeedDatabase == false;
				return true;
			else:
				return false;
			endif;
		endif;
	}

	/* End Section */

	/* Section Articles and compose_news */

	public static
	function check_if_habbo_exists ()
	{
		if (!empty($_SESSION['HabboData']) && $_SESSION['HabboData'] != ""):
			self::$Habbo = unserialize ( $_SESSION['HabboData'] );
			return true;
		else:
			return false;
		endif;
	}

	public static
	function user_login ( $Username, $Password, $Newbie = 0 )
	{
		if (!StorageHabbo::$NeedDatabase):
			StorageHabbo::$NeedDatabase = true;
		endif;
		if (!StorageHabbo::database_is_available ( 1 )):
			echo xEcho ( "Exception on Violation database: function user_login" );
		else:
			if (!StorageHabbo::check_if_habbo_exists ()):
				$Username = strip_tags ( htmlentities ( addslashes ( $Username ) ) );
				$Password = htmlentities ( addslashes ( md5 ( $Password ) ) );
				$Query    = DatabaseAdapter::query ( "SELECT id FROM users WHERE username = \"$Username\" AND password = \"$Password\" OR mail = \"$Username\" AND password = \"$Password\" LIMIT 1" );
				if (DatabaseAdapter::num_rows ( $Query ) == 1):
					$Query = DatabaseAdapter::query ( "SELECT id FROM users WHERE username = \"$Username\" AND password = \"$Password\" OR mail = \"$Username\" AND password = \"$Password\" LIMIT 1" );
					$Fetch = DatabaseAdapter::fetch_all ( $Query );
					$Id    = $Fetch['id'];
					StorageHabbo::habbo ( $Id );
					if (!StorageHabbo::check_if_news_exists ())
						StorageHabbo::compose_news ( 0 );
					if ($Newbie):
						redirect ( '/welcome/' );
						$_SESSION['IsNewbie'] = "okayme";
					else:
						redirect ( '/me/' );
					endif;
				else:
					$_POST['HotelMessage'] = array ('Error' => 'Your Password or Username is Wrong..');
				endif;
			else:
				redirect ( '/me/' );
			endif;
		endif;
	}

	public static
	function habbo ( $Id )
	{
		if (!StorageHabbo::check_if_habbo_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (!StorageHabbo::database_is_available ( 1 )):
				echo xEcho ( "Exception on Violation database: function habbo" );
			else:
				$Query = DatabaseAdapter::query ( "SELECT * FROM users WHERE id = $Id LIMIT 1" );
				$Fetch = DatabaseAdapter::fetch_all ( $Query );
				if ($Fetch['rank'] >= 7):
					$IsAdmin = true;
				else:
					$IsAdmin = false;
				endif;
				self::$Habbo           = new Habbo( $Id, $Fetch['username'], $Fetch['mail'], $Fetch['gender'], $Fetch['motto'], $Fetch['credits'], $Fetch['activity_points'], "127.0.0.1", "Default", $Fetch['look'], $IsAdmin );
				$_SESSION['HabboData'] = serialize ( self::$Habbo );
			endif;
		endif;
	}

	public static
	function check_if_news_exists ()
	{
		if (!empty($_SESSION['NewsData']) && $_SESSION['NewsData'] != ""):
			return true;
		else:
			return false;
		endif;
	}

	public static
	function compose_news ( $Return = 1 )
	{
		if (!StorageHabbo::check_if_news_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (!StorageHabbo::database_is_available ( 1 )):
				echo xEcho ( "Exception on Violation database: function compose_news" );
			else:
				$Query = DatabaseAdapter::query ( "SELECT id,title,image,text FROM cms_news ORDER BY id ASC;" );
				$Code  = "<div id=\"promo-box\">";
				$Code .= "<div id=\"promo-bullets\"></div>";
				$DataArticle = '';
				while ($Row = DatabaseAdapter::fetch_all ( $Query ))
				{
					$Text = mb_strimwidth ( $Row['text'], 0, 20, "..." );
					$Id   = $Row['id'];
					$Code .= "<div class=\"promo-container\" style=\"background-image: url(" . $Row["image"] . ")\">";
					$Code .= "<div class=\"promo-content-container\">";
					$Code .= "<div class=\"promo-content\">";
					$Code .= "<div class=\"title\"><a href=\"{{hotel_url}}/articles/?articleid=$Id\" style=\"text-decoration: none;color: white;\">" . $Row["title"] . "</a></div>";
					$Code .= "<div class=\"body\">" . strip_tags ( html_entity_decode ( $Text ) ) . "</div>";
					$Code .= "</div>";
					$Code .= "</div>";
					$DataArticle[$Id] = array ('id' => $Row['id'], 'title' => $Row['title'], 'text' => $Row['text']);
					if (StorageHabbo::get_habbo ( 'user_is_admin' ) == true):
						$Code .= "<div class=\"promo-link-container\">\n";
						$Code .= "<div class=\"enter-hotel-btn\">\n";
						$Code .= "<div class=\"open enter-btn\">\n";
						$Code .= "<a style=\"padding: 0 8px 0 19px;padding: 0 8px 0 19px;border-style: solid;border-width: 0px;cursor: pointer;font-weight: normal;line-height: normal;margin: 0 0 1.11111rem;position: relative;text-decoration: none;text-align: center;-webkit-appearance: none;display: inline-block;padding-top: 0.88889rem;padding-right: 1.77778rem;padding-bottom: 0.94444rem;padding-left: 1.77778rem;font-size: 0.88889rem;color: white;background-color: #43ac6a;border-color: #368a55;border-radius: 3px;background-image: none;height: auto;top: 5px;\" href=\"{{hotel_url}}/me/deletenews/?deleteid=" . $Row['id'] . "\">Delete Article</a>\n";
						$Code .= "</div>\n";
						$Code .= "</div>\n";
						$Code .= "</div>\n";
					endif;
					$Code .= "</div>";
				}
				$_SESSION['compose_article'] = $DataArticle;
				if (StorageHabbo::get_habbo ( 'user_is_admin' ) == true):
					$Code .= "<div class=\"promo-container\" style=\"background-image: url(http://3.bp.blogspot.com/-nN77Fg18Ygs/Ub9U4ADsggI/AAAAAAAADsk/PF9JAkeZ6uI/s1600/11.png)\">";
					$Code .= "<div class=\"promo-content-container\">";
					$Code .= "<div class=\"promo-content\">";
					$Code .= "<div class=\"title\">Welcome Admin</div>";
					$Code .= "<div class=\"body\">Do you Want to Create a compose_news?</div>";
					$Code .= "</div>";
					$Code .= "</div>";
					$Code .= "<div class=\"promo-link-container\">\n";
					$Code .= "<div class=\"enter-hotel-btn\">\n";
					$Code .= "<div class=\"open enter-btn\">\n";
					$Code .= "<a style=\"padding: 0 8px 0 19px;padding: 0 8px 0 19px;border-style: solid;border-width: 0px;cursor: pointer;font-weight: normal;line-height: normal;margin: 0 0 1.11111rem;position: relative;text-decoration: none;text-align: center;-webkit-appearance: none;display: inline-block;padding-top: 0.88889rem;padding-right: 1.77778rem;padding-bottom: 0.94444rem;padding-left: 1.77778rem;font-size: 0.88889rem;color: white;background-color: #43ac6a;border-color: #368a55;border-radius: 3px;background-image: none;height: auto;top: 5px;\" href=\"{{hotel_url}}/createnews/\">Create a Article</a>\n";
					$Code .= "</div>\n";
					$Code .= "</div>\n";
					$Code .= "</div>\n";
					$Code .= "</div>";
				endif;
				$Code .= "</div>";
				$Code .= "<script type=\"text/javascript\">";
				$Code .= "document.observe(\"dom:loaded\", function() { PromoSlideShow.init(); });";
				$Code .= "</script>";
				$_SESSION['NewsData'] = $Code;
				if ($Return):
					return $Code;
				endif;
			endif;
		else:
			if ($Return):
				return $_SESSION['NewsData'];
			endif;
		endif;
	}

	/* End Section */

	/* Section promos from Index */

	public static
	function get_habbo ( $Var = "none" )
	{
		if (StorageHabbo::check_if_habbo_exists ()):
			if ($Var == "none"):
				$Vars = get_class_vars ( get_class ( self::$Habbo ) );
				foreach ($Vars as $Name => $Value):
					$Value             = self::$Habbo->get_habbo ( $Name );
					$HabboArray[$Name] = $Value;
				endforeach;
				return $HabboArray;
			else:
				return self::$Habbo->get_habbo ( $Var );
			endif;
		else:
			return "not_logged";
		endif;
	}

	public static
	function compose_article ()
	{
		if (isset($_SESSION['compose_article']) && is_array ( $_SESSION['compose_article'] )):
			$Code = '';
			$Code .= "<div id=\"column1\" class=\"column\">\n";
			$Code .= "<div class=\"cbb clearfix red\">\n<h2 class=\"title\">compose_news</h2>";
			$Code .= "<div id=\"article-archive\">\n";
			$Code .= "<h2>Articles</h2>\n";
			$Code .= "<ul>\n";
			if (isset($_GET['articleid']) && is_numeric ( $_GET['articleid'] )):
				$ArticleId = $_GET['articleid'];
			else:
				redirect ( '/me/' );
			endif;
			$ArticleData = $_SESSION['compose_article'];
			foreach ($ArticleData as $Key)
			{
				$Id    = $Key['id'];
				$Title = $Key['title'];
				$Text  = $Key['text'];
				if ($Id == $ArticleId):
					$Code .= "<li>$Title</li>";
				else:
					$Code .= "<li><a href=\"{{hotel_url}}/articles/?articleid=$Id\">$Title</a></li>";
				endif;
			}
			$ArticleNews = $ArticleData[$ArticleId];
			$Title       = $ArticleNews['title'];
			$Text        = $ArticleNews['text'];
			$Text        = wordwrap ( $Text, 70, "<br/>\n", true );
			$Text        = html_entity_decode ( $Text );
			$Summary     = mb_substr ( $Text, 0, 100 );
			$Summary     = wordwrap ( $Summary, 70, "<br/>\n", true );
			$Summary     = strip_tags ( html_entity_decode ( $Summary ) );
			$Code .= "</ul>\n";
			$Code .= "</div>\n";
			$Code .= "</div>\n";
			$Code .= "</div>\n";
			$Code .= "<div id=\"column2\" class=\"column\">\n";
			$Code .= "<div class=\"cbb clearfix notitle\">\n";
			$Code .= "<div id=\"article-wrapper\">\n";
			$Code .= "<h2>$Title</h2>\n";
			$Code .= "<div class=\"article-meta\">By The Staff</div>";
			$Code .= "<p class=\"summary\">$Summary...</p>";
			$Code .= "<p>$Text</p>";
			$Code .= "<div class=\"article-images clearfix\">\n";
			$Code .= "</div>\n";
			$Code .= "<div class=\"article-body\"><p><font face=\"Verdana\" size=\"1\"><b>The Staff</b></font></p></div><font face=\"Verdana\" size=\"1\">\n";
			$Code .= "<script type=\"text/javascript\" language=\"Javascript\">\n";
			$Code .= "document.observe(\"dom:loaded\", function() {\n";
			$Code .= "$$('.article-images a').each(function(a) {\n";
			$Code .= "Event.observe(a, 'click', function(e) {\n";
			$Code .= "Event.stop(e);\n";
			$Code .= "body_overlay.lightbox(a.href, \"Image is loading\");\n";
			$Code .= "});\n";
			$Code .= "});\n";
			$Code .= "$$('a.article-3435').each(function(a) {\n";
			$Code .= "a.replace(a.innerHTML);\n";
			$Code .= "});\n";
			$Code .= "});\n";
			$Code .= "</script>\n";
			$Code .= "</font></div><font face=\"Verdana\" size=\"1\">\n";
			$Code .= "</font></div>\n";
			$Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
			$Code .= "</div>\n";
			$Code .= "</div>\n";
			return $Code;
		else:
			return 'NOPE';
		endif;
	}

	/* End Section */

	/* Section staffs page */

	public static
	function add_news ( $Array )
	{
		if (!StorageHabbo::$NeedDatabase):
			StorageHabbo::$NeedDatabase = true;
		endif;
		if (!StorageHabbo::database_is_available ( 1 )):
			echo xEcho ( "Exception on Violation database: function add_news" );
		else:
			$Title = htmlentities ( addslashes ( $Array['title'] ) );
			$Image = htmlentities ( addslashes ( $Array['image'] ) );
			$Text  = htmlentities ( addslashes ( $Array['text'] ) );
			DatabaseAdapter::query ( "INSERT INTO cms_news (title,image,text) VALUES ('$Title','$Image','$Text');" );
			$_POST['HotelMessage'] = array ('Success' => 'Article Created Succefully.. Reenter on hotel to See Changes.');
		endif;
	}

	public static
	function delete_news ( $ArticleId )
	{
		if (!is_numeric ( $ArticleId )):
			echo xEcho ( "Fucked Idiot Hacker Go to Hell" );
		else:
			if (StorageHabbo::get_habbo ( 'user_is_admin' ) == true):
				if (!StorageHabbo::$NeedDatabase):
					StorageHabbo::$NeedDatabase = true;
				endif;
				if (!StorageHabbo::database_is_available ( 1 )):
					echo xEcho ( "Exception on Violation database: function delete_news" );
				else:
					DatabaseAdapter::query ( "DELETE FROM cms_news WHERE id = $ArticleId;" );
					$_POST['HotelMessage'] = array ('Success' => 'Article Deleted Succefully.. Reenter on hotel to See Changes.');
				endif;
			else:
				$_POST['HotelMessage'] = array ('Error' => 'You Are Not Administrator!');
			endif;
		endif;
	}

	/* End Section */

	/* Section habbo Data and hotel Data */

	public static
	function promos ( $Return = 1 )
	{
		if (!StorageHabbo::check_if_promo_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (StorageHabbo::database_is_available ( 1 )):
				$Query = DatabaseAdapter::query ( "SELECT username,motto,look FROM users ORDER BY RAND()" );
				$Nums  = DatabaseAdapter::num_rows ( $Query );
				$Query = DatabaseAdapter::query ( "SELECT username,motto,look FROM users ORDER BY RAND() LIMIT $Nums" );
				$Id    = 1;
				while ($Row = DatabaseAdapter::fetch_all ( $Query ))
				{
					$Array[$Id] = array ('id' => $Id, 'username' => $Row['username'], 'motto' => $Row['motto'], 'look' => $Row['look']);
					$Id++;
				}
				$_SESSION['PromoData'] = $Array;
				$_SESSION['PromoNums'] = $Nums;
			else:
				echo xEcho ( "Exception on Violation database: function promos" );
				$_SESSION['PromoData'] = '';
				$_SESSION['PromoNums'] = 0;
			endif;
			if ($Return):
				return $_SESSION['PromoData'];
			endif;
		else:
			if ($Return):
				return $_SESSION['PromoData'];
			endif;
		endif;
	}

	public static
	function check_if_promo_exists ()
	{
		if (!empty($_SESSION['PromoData']) && $_SESSION['PromoData'] != ""):
			return true;
		else:
			return false;
		endif;
	}

	public static
	function staffs ( $Return = 1 )
	{
		if (!StorageHabbo::check_if_staff_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (StorageHabbo::database_is_available ( 1 )):
				$Code  = "";
				$Ranks = array (0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => 'Moderators', 6 => 'Administrators', '7' => 'Managers', 8 => 'Directors', 9 => 'Owners', 10 => 'Creators');
				for ($R = 10; $R >= 5; $R--)
				{
					$Rank = $Ranks[$R];
					$Code .= "<div  class=\"cbb clearfix\" id=\"avatar-selector-habblet\">";
					$Code .= "<h2 class=\"title\">$Rank</h2><ul>\n";
					$Odd   = "even";
					$Query = DatabaseAdapter::query ( "SELECT username,look,gender,motto FROM users WHERE rank = '$R'" );
					while ($Row = DatabaseAdapter::fetch_all ( $Query ))
					{
						$Odd = ($Odd == "even") ? "odd" : "even";
						$Code .= "<li class=\"$Odd\">\n";
						$Code .= "<img class=\"avatar-image\" src=\"http://habbo.de/habbo-imaging/avatarimage?figure={$Row["look"]}&size=s&direction=4&head_direction=4&gesture=sml\" width=\"33\" height=\"56\"/>\n";
						$Code .= "<div class=\"avatar-info\">\n";
						$Code .= "<div class=\"avatar-info-container\">\n";
						$Code .= "<div class=\"avatar-name\">{$Row["username"]}</div>\n";
						$Code .= "<div class=\"avatar-info\">{$Row["motto"]}\n";
						$Code .= "</div>\n";
						$Code .= "</div>\n";
						$Code .= "</div>\n";
						$Code .= "</li>\n";
					}
					$Code .= "</ul></div>";
				}
				$_SESSION['StaffData'] = $Code;
			else:
				echo xEcho ( "Exception on Violation database: function staffs" );
				$_SESSION['StaffData'] = null;
			endif;
			if ($Return):
				return $_SESSION['StaffData'];
			endif;
		else:
			if ($Return):
				return $_SESSION['StaffData'];
			endif;
		endif;
	}

	public static
	function check_if_staff_exists ()
	{
		if (!empty($_SESSION['StaffData']) && $_SESSION['StaffData'] != ""):
			return true;
		else:
			return false;
		endif;
	}

	/* End Section */

	/* Section GetData */

	public static
	function hotel ( $IfReturn = 0 )
	{
		if (!StorageHabbo::check_if_hotel_exists ()):
			if (!StorageHabbo::$NeedDatabase):
				StorageHabbo::$NeedDatabase = true;
			endif;
			if (!StorageHabbo::database_is_available ( 1 )):
			else:
				$QuerA = DatabaseAdapter::query ( "SELECT * FROM server_status" );
				$FetcA = DatabaseAdapter::fetch_all ( $QuerA );
				$QuerB = DatabaseAdapter::query ( "SELECT id FROM users" );
				$NumeB = DatabaseAdapter::num_rows ( $QuerB );
				if ($FetcA['users_online'] == 0)
					$FetcA['users_online'] = "None";
				if ($NumeB == 0)
					$NumeB = "None";
				self::$Hotel           = new Hotel( $FetcA['users_online'], $FetcA['server_ver'], $NumeB );
				$_SESSION['HotelData'] = serialize ( self::$Hotel );
				if ($IfReturn == 1):
					return array ('online_users' => $FetcA['users_online'], 'registered_users' => $NumeB);
				endif;
			endif;
		endif;
	}

	public static
	function check_if_hotel_exists ()
	{
		if (!empty($_SESSION['HotelData'])):
			self::$Hotel = unserialize ( $_SESSION['HotelData'] );
			return true;
		else:
			return false;
		endif;
	}

	/* End Section */

	public static
	function get_hotel ( $Var = "none" )
	{
		if (StorageHabbo::check_if_hotel_exists ()):
			if ($Var == "none"):
				$Vars = get_class_vars ( get_class ( self::$Hotel ) );
				foreach ($Vars as $Name => $Value)
				{
					$Value             = self::$Hotel->get_hotel ( $Name );
					$HotelArray[$Name] = $Value;
				}
				return $HotelArray;
			else:
				return self::$Hotel->get_hotel ( $Var );
			endif;
		else:
			echo 'hotel Not Started';
			return "not_logged";
		endif;
	}

	public static
	function destroy ()
	{
		self::$Habbo        = null;
		self::$Hotel        = null;
		self::$NeedDatabase = true;
		session_start ();
		$_SESSION['NeedRefresh'] = true;
	}

}

function redirect ( $Url = '' )
{
	$Base = explode ( "/", $_SERVER['SCRIPT_NAME'] );
	for ($i = 0; $i < count ( $Base ); $i++)
		unset($Base[sizeof ( $Base ) - 1]);
	$Base = implode ( "/", $Base );
	header ( "Location: " . $Base . $Url );
}

function link_to ( $Url = '' )
{

	$Base = explode ( "/", $_SERVER['SCRIPT_NAME'] );
	for ($i = 0; $i < count ( $Base ); $i++)
		unset($Base[sizeof ( $Base ) - 1]);
	$Base = implode ( "/", $Base );
	return $Base . $Url;
}

function clink_to ( $Url = '' )
{

	$Base = explode ( "/", $_SERVER['SCRIPT_NAME'] );
	for ($i = 0; $i < count ( $Base ); $i++)
		unset($Base[sizeof ( $Base ) - 1]);
	$Base = implode ( "/", $Base );
	$Url  = 'http://' . $_SERVER['HTTP_HOST'] . $Base . $Url;
	return $Url;
}

function render_content ( $template = 'index' )
{
	ob_start ();
	include_once (ROOT_PATH . "/templates/" . $template . ".php");
	$content = ob_get_contents ();
	ob_end_clean ();
	return $content;

}

function xEcho ( $Text, $SpecialType = 'none' )
{
	switch ($SpecialType)
	{
		case 'none':
			$Type = '/index/';
			break;
		case 'logout':
			$Type = '/action/logout/';
			break;
	}
	$Code = "<style>#login-errors {width: 100%;height: auto;background-color: #fc6621;color: #fff;font-size: 17px;font-weight: bold;text-align: center;line-height: 35px;}</style>\n";
	$Code .= "<div id=\"login-errors\">\n";
	$Code .= $Text . "<a id=\"forgot-password-localization-link\" style=\"font-size:13pt;color:black;\" href=\"?$Type\"> Go Back.</a>" . "\n";
	$Code .= "</div>\n";
	return $Code;
}


if (isset($_SESSION['NeedRefresh'])):
	header ( "Refresh:0" );
	$_SESSION['NeedRefresh'] = null;
else:
	$habbo_object = StorageHabbo::get_habbo ();
	$page         = new Page( $database_settings, $hotel_settings, $habbo_object );
	$page_content = $page->set_page ();
	$make_page    = $page->make_page ( $page_content['E'], $page_content['D'] );
	if (Page::sub_header_started () == 'not_started'):
		Page::start_sub_header ( $subheader_array );
	endif;
	$database = $page->database ( $database_settings );
	if ($database):
		$Result = $page->serialize ( $make_page );
		if (StorageHabbo::database_is_available ( 1 )):
			DatabaseAdapter::close ();
		endif;
		echo $Result;
	else:
		if (StorageHabbo::database_is_available ( 1 )):
			DatabaseAdapter::close ();
		endif;
	endif;

endif;
echo '<!-- Loaded in ' . (microtime ( true ) - TIME_GOIN) . ' seconds -->';
?>